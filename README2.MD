# Deployment steps

In order to deploy

## Install dependencies 

```
npm install 
```

## Deploy to a specific network

```
export MNEMONIC="the mnemonice here"
export HD_WALLET_API_URL="https://someurl.com"

./node_modules/.bin/truffle migrate --network [value]

```

The [value] here can be: rinkeby, bsc, avalanche, live, polygon


**NOTES**: live here represents the eth mainnet
            don't put the angle brackets just the `value`


# Modifiying the 2_deploy_contracts.js

Go inside the file 2_deploy_contracts.js and update the list of addresses to use as signers,
and inside that file there is a variable named `salt` update its value in order to use a different salt 

