const WalletSimple = artifacts.require('WalletSimple');
const WalletFactory = artifacts.require('WalletFactory');
const Forwarder = artifacts.require('Forwarder');
const ForwarderFactory = artifacts.require('ForwarderFactory');
const util = require('ethereumjs-util');

module.exports = async function (deployer) {

  const addresses = [
    "0x1950161b559FA0083daf54EF1272ceB59aABD2dd", // parent addr
    "0x1346f177Caf4B5EEC150624143840d3962Bcbf2b", // signer 1
    "0x7db2E643679BF0393c6631dC41b6F8206cBBe8D6" // signer 2
  ]

  const salt = '0x1234';
  const inputSalt = util.setLengthLeft(
    Buffer.from(util.stripHexPrefix(salt), 'hex'),
    32
  );

  await deployer.deploy(WalletSimple);
  const walletSimple = await WalletSimple.deployed();

  const walletFactory = await deployer.deploy(WalletFactory, walletSimple.address);
  await walletFactory.WalletCreated((e, v) => {
    if (v){
      console.log("WalletCreated ==>> ", v.returnValues);
    }
  });

  walletFactory.createWallet([addresses[0], addresses[1], addresses[2]], inputSalt);

  await deployer.deploy(Forwarder);
  const forwarder = await Forwarder.deployed();

  const forwarderFactory = await deployer.deploy(ForwarderFactory, forwarder.address);

  await forwarderFactory.ForwarderCreated((e, v) => {
    if (v){
      console.log("WalletCreated ==>> ", v.returnValues);
    }
  });
  await forwarderFactory.createForwarder(addresses[0], inputSalt);
  
};
