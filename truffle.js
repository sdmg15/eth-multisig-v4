require('dotenv').config();
const HDWalletProvider = require('@truffle/hdwallet-provider');
const { ETHERSCAN_API_KEY } = process.env;

const HD_WALLET_API_URL = process.env.HD_WALLET_API_URL;
const MNEMONIC = process.env.MNEMONIC;

module.exports = {
  networks: {
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*', // Match any network id
      gas: 5800000,
      websockets: true,
      from: "0xA2FC5e33Dba722086CEed56071dbc08A01050084",
    },
    mainnet: {
      provider: function () {
        return new HDWalletProvider(MNEMONIC, HD_WALLET_API_URL);
      },
      network_id: '*',
      gas: 5800000,
      websockets: true
    },

    rinkeby:  {
      provider: function () {
        return new HDWalletProvider(MNEMONIC, HD_WALLET_API_URL);
      },
      network_id: '*',
      gas: 5800000,
      websockets: true
    },

    avalanche:  {
      provider: function () {
        return new HDWalletProvider(MNEMONIC, HD_WALLET_API_URL);
      },
      network_id: '*',
      gas: 5800000,
      websockets: true
    },

    bsc:  {
      provider: function () {
        return new HDWalletProvider(MNEMONIC, HD_WALLET_API_URL);
      },
      network_id: '*',
      gas: 5800000,
      websockets: true
    },

    polygon:  {
      provider: function () {
        return new HDWalletProvider(MNEMONIC, HD_WALLET_API_URL);
      },
      network_id: '*',
      gas: 5800000,
      websockets: true
    },

  },
  compilers: {
    solc: {
      version: '0.8.10',
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  },
  plugins: ['solidity-coverage', 'truffle-plugin-verify'],
  api_keys: {
    etherscan: ETHERSCAN_API_KEY ?? ''
  }
};
